package net.gerdsmeier.spring.validation.dto.groups;

/**
 * Indicates an existing entity.
 */
public interface Update {
}
