package net.gerdsmeier.spring.validation.dto.groups;

/**
 * Indicates a new entity.
 */
public interface Create {
}
