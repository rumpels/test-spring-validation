package net.gerdsmeier.spring.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.gerdsmeier.spring.validation.dto.groups.Create;
import net.gerdsmeier.spring.validation.dto.groups.Update;

import javax.validation.constraints.*;
import java.util.UUID;

/**
 * Task dto.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    /**
     * Id.
     */
    @Null(groups = Create.class)
    private UUID id;

    /**
     * Name.
     */
    @NotNull(groups = Create.class)
    @Size(min = 1, max = 20)
    private String name;

    /**
     * Duration.
     */
    @Positive
    @NotNull(groups = Create.class)
    @Min(value = 10, groups = Update.class)
    private Long duration = 1L;

    @NotNull
    private String description = "default";

    /**
     * CTOR.
     *
     * @param id       id
     * @param name     name
     * @param duration duration
     */
    public TaskDto(
            final UUID id,
            final String name,
            final Long duration
    ) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }
}
