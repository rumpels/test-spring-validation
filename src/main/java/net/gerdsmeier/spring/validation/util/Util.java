package net.gerdsmeier.spring.validation.util;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * General util class.
 */
public final class Util {

    /**
     * Util class.
     */
    private Util() {
    }

    /**
     * Generate a uuid from a name.
     *
     * @param name name
     * @return uuid
     */
    public static UUID namedId(final String name) {
        return UUID.nameUUIDFromBytes(name.getBytes(StandardCharsets.UTF_8));
    }
}
