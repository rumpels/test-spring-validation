package net.gerdsmeier.spring.validation.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import ma.glasnost.orika.MapperFacade;
import net.gerdsmeier.spring.validation.dto.TaskDto;
import net.gerdsmeier.spring.validation.util.Util;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TaskService {
    private final ConcurrentHashMap<UUID, ManagedTask> dataMap = new ConcurrentHashMap<>();

    @Autowired
    private MapperFacade mapperFacade;

    public synchronized TaskDto createTask(@NotNull final TaskDto task) {
        final ManagedTask managedTask = mapperFacade.map(task, ManagedTask.class);
        final UUID key = Util.namedId(task.getName());
        managedTask.setId(key);

        final boolean created = this.dataMap.putIfAbsent(key, managedTask) == null;

        return created ? mapToDto(managedTask) : null;
    }

    public synchronized TaskDto getTask(@NotNull final UUID id) {
        return mapToDto(this.dataMap.get(id));
    }

    public synchronized TaskDto updateTask(@NotNull final UUID id, @NotNull final TaskDto task) {
        final ManagedTask existingTask = this.dataMap.get(id);
        if (existingTask != null) {
            mapperFacade.map(task, existingTask);

            return mapToDto(existingTask);
        }
        return null;
    }

    public synchronized boolean deleteTask(@NotNull final UUID id) {
        return this.dataMap.remove(id) != null;
    }

    public synchronized void clear() {
        this.dataMap.clear();
    }

    private TaskDto mapToDto(final ManagedTask managedTask) {
        if (managedTask == null) {
            return null;
        }

        return mapperFacade.map(managedTask, TaskDto.class);
    }

    @Data
    @AllArgsConstructor
    public static class ManagedTask {
        private UUID id;
        private String name;
        private Long duration;
        private String description;
    }
}
