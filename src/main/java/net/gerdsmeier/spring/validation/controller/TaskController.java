package net.gerdsmeier.spring.validation.controller;

import ma.glasnost.orika.MapperFacade;
import net.gerdsmeier.spring.validation.dto.TaskDto;
import net.gerdsmeier.spring.validation.dto.groups.Create;
import net.gerdsmeier.spring.validation.dto.groups.Update;
import net.gerdsmeier.spring.validation.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class TaskController {

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private TaskService taskService;

    @PostMapping("/task")
    public ResponseEntity<TaskDto> create(
            @Validated(Create.class) @RequestBody final TaskDto task
    ) {
        final TaskDto newTask = taskService.createTask(task);
        final boolean created = newTask != null;

        return created
                ? new ResponseEntity<>(newTask, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @GetMapping("/task/{taskId}")
    public ResponseEntity<TaskDto> read(@PathVariable final UUID taskId) {
        final TaskDto task = taskService.getTask(taskId);
        final boolean found = task != null;

        return found
                ? new ResponseEntity<>(task, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/task/{taskId}")
    public ResponseEntity<TaskDto> update(
            @PathVariable final UUID taskId,
            @Validated(Update.class) @RequestBody final TaskDto task
    ) {
        final TaskDto taskDto = taskService.updateTask(taskId, task);

        final boolean updated = taskDto != null;

        return updated
                ? new ResponseEntity<>(taskDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/task/{taskId}")
    public ResponseEntity<?> delete(@PathVariable final UUID taskId) {
        final boolean deleted = taskService.deleteTask(taskId);
        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
