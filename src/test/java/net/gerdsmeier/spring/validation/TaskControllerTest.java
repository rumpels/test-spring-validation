package net.gerdsmeier.spring.validation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.gerdsmeier.spring.validation.dto.TaskDto;
import net.gerdsmeier.spring.validation.service.TaskService;
import net.gerdsmeier.spring.validation.util.Util;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TaskControllerTest {

    @Autowired
    private ObjectMapper jacksonMapper;

    @Autowired
    private TaskService taskService;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.taskService.clear();
    }

    /*
     * CREATE
     */
    @Test
    public void create_validTask() throws Exception {
        final ObjectNode expectedResult = jacksonMapper.createObjectNode();
        expectedResult.put("id", Util.namedId("test").toString());
        expectedResult.put("name", "test");
        expectedResult.put("duration", 100);
        expectedResult.put("description", "default");


        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("name", "test");
        taskPayload.put("duration", 100);

        this.mockMvc.perform(
                post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json(expectedResult.toString())
        );
    }

    @Test
    public void create_existingTaskName() throws Exception {
        taskService.createTask(new TaskDto(null, "newTask", 20L));

        final ObjectNode task = jacksonMapper.createObjectNode();
        task.put("name", "newTask");
        task.put("duration", 100);

        this.mockMvc.perform(
                post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(task.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isConflict()
        );
    }

    @Test
    public void create_validTaskOverrideDefault() throws Exception {
        final ObjectNode expectedResult = jacksonMapper.createObjectNode();
        expectedResult.put("id", Util.namedId("test").toString());
        expectedResult.put("name", "test");
        expectedResult.put("duration", 100);
        expectedResult.put("description", "test"); // to be overridden

        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("name", "test");
        taskPayload.put("duration", 100);
        taskPayload.put("description", "test");

        this.mockMvc.perform(
                post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json(expectedResult.toString())
        );
    }


    @Test
    public void create_invalidTaskIdSet() throws Exception {
        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("id", Util.namedId("test").toString()); // this is not allowed - see dto
        taskPayload.put("name", "test");
        taskPayload.put("duration", 10);

        this.mockMvc.perform(
                post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isBadRequest()
        ).andExpect(
                content().string(isEmptyOrNullString())
        );
    }

    /*
     * READ
     */
    @Test
    public void read_existingTask() throws Exception {
        final TaskDto newTask = taskService.createTask(new TaskDto(null, "newTask", 20L));

        this.mockMvc.perform(
                get("/task/{taskId}", newTask.getId())
        ).andDo(
                print()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json(jacksonMapper.writeValueAsString(newTask))
        );

        Assert.assertEquals(Util.namedId("newTask"), newTask.getId());
    }

    @Test
    public void read_missingTask() throws Exception {
        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("id", Util.namedId("test").toString());
        taskPayload.put("name", "test");
        taskPayload.put("duration", 10);

        this.mockMvc.perform(
                get("/task/{taskId}", Util.namedId("unknown")) // its doesn't exists
        ).andDo(
                print()
        ).andExpect(
                status().isNotFound()
        );
    }

    /*
     * UPDATE
     */
    @Test
    public void update_existingTask() throws Exception {
        final TaskDto newTask = taskService.createTask(new TaskDto(null, "newTask", 20L));

        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("duration", 100L);

        this.mockMvc.perform(
                put("/task/{taskId}", newTask.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json(
                        jacksonMapper.writeValueAsString(new TaskDto(newTask.getId(), "newTask", 100L))
                )
        );

        Assert.assertEquals(100L, taskService.getTask(newTask.getId()).getDuration().longValue());
        Assert.assertEquals("newTask", taskService.getTask(newTask.getId()).getName());
    }

    public void update_missingTask() throws Exception {
        final ObjectNode taskPayload = jacksonMapper.createObjectNode();

        this.mockMvc.perform(
                put("/task/{taskId}", Util.namedId("unknown"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isNotFound()
        );
    }

    @Test
    public void update_existingTaskValidationError() throws Exception {
        final Long TOO_SMALL_DURATION = 5L;

        final TaskDto newTask = taskService.createTask(new TaskDto(null, "newTask", 20L));

        final ObjectNode taskPayload = jacksonMapper.createObjectNode();
        taskPayload.put("duration", TOO_SMALL_DURATION);

        this.mockMvc.perform(
                put("/task/{taskId}", newTask.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(taskPayload.toString())
        ).andDo(
                print()
        ).andExpect(
                status().isBadRequest()
        );

        Assert.assertEquals(20L, taskService.getTask(newTask.getId()).getDuration().longValue());
    }

    /*
     * DELETE
     */
    @Test
    public void delete_existingTask() throws Exception {
        final TaskDto newTask = taskService.createTask(new TaskDto(null, "newTask", 20L));

        this.mockMvc.perform(
                delete("/task/{taskId}", newTask.getId())
        ).andDo(
                print()
        ).andExpect(
                status().isOk()
        );

        Assert.assertTrue(taskService.getTask(newTask.getId()) == null);
    }

    @Test
    public void delete_missingTask() throws Exception {
        this.mockMvc.perform(
                delete("/task/{taskId}", Util.namedId("unknown"))
        ).andDo(
                print()
        ).andExpect(
                status().isNotFound()
        );
    }


}